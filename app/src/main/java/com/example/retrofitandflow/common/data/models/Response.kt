package com.example.retrofitandflow.common.data.models

data class Response<T>(
    val data: T,
    val status: String,
    val message: String
)
