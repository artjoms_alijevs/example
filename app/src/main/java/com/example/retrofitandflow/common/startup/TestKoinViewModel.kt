package com.example.retrofitandflow.common.startup

import androidx.lifecycle.ViewModel
import com.example.retrofitandflow.common.AppStorage
import com.example.retrofitandflow.common.data.repository.TokenManager

class TestMyKoinViewModel(private val appStorage: AppStorage) : ViewModel() {
    val id get() = appStorage.selectedClientId
    var test = "test"

    fun setId() {
        appStorage.selectedClientId = 5678
    }
}