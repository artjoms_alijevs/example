package com.example.retrofitandflow.common.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class TestItem(
    val id: Int,
    @SerializedName("employee_name") val name: String
): Parcelable