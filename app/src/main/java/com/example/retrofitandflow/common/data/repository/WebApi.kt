package com.example.retrofitandflow.common.data.repository

import com.example.retrofitandflow.common.data.models.Response
import com.example.retrofitandflow.common.data.models.TestItem
import retrofit2.http.GET

interface WebApi {

    @GET("employees")
    suspend fun getItems(): Response<List<TestItem>>

    @GET("employees/2")
    suspend fun updateSession(): Response<TestItem>
}