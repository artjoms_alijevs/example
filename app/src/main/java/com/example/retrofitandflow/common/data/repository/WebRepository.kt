package com.example.retrofitandflow.common.data.repository

import android.content.Context
import com.example.retrofitandflow.common.SessionStorage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.component.inject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class WebRepository: BaseRepository() {

    private val api: WebApi = provider.retrofit().create(WebApi::class.java)

    suspend fun getItems() = flow { emit(api.getItems()) }
        .map { response -> response.data }
        .flowOn(Dispatchers.IO)

    suspend fun getItems2() = flow { emit(api.getItems()) }
        .map {
            delay(2000)
            return@map it.data
        }
        .flowOn(Dispatchers.IO)
}

open class BaseRepository: KoinComponent {
    val provider: RetrofitProvider by inject()
}

class RetrofitProvider(private val tokenManager: TokenManager) {

    fun retrofit(): Retrofit = Retrofit.Builder()
        .baseUrl("http://dummy.restapiexample.com/api/v1/")
        .client(
            OkHttpClient.Builder()
                .addInterceptor { chain ->
                    val request = chain.request()
                        .newBuilder()
                        .addHeader("x-apikey", tokenManager.tokken)
                        .build()
                    val response = chain.proceed(request)
                    // Special error codes can be catched here
                    response
                }
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.HEADERS))
                .build())
        .addConverterFactory(GsonConverterFactory.create())
        .build()


}

class TokenManager(val context: Context) {
    var tokken = "sessionValues"

    fun save() {

    }

    fun restore() {

    }
}