package com.example.retrofitandflow.common

import android.app.Application
import com.example.retrofitandflow.common.data.repository.RetrofitProvider
import com.example.retrofitandflow.common.data.repository.TokenManager
import com.example.retrofitandflow.common.startup.TestMyKoinViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.GlobalContext.startKoin
import org.koin.dsl.module

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        setupDi()
    }

    private fun setupDi() {
        startKoin {
            androidLogger()

            androidContext(this@App)
            modules(appModule)
        }
    }

    override fun onTerminate() {
        super.onTerminate()
        SessionStorage.save(this)
    }
}

val appModule = module {

    single { TokenManager(androidContext()) }
    single { RetrofitProvider(get()) }
    single { AppStorage() }

    viewModel { TestMyKoinViewModel(get()) }
}

class AppStorage {
    var selectedClientId: Int? = 123
}