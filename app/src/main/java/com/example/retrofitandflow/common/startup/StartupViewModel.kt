package com.example.retrofitandflow.common.startup

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.retrofitandflow.common.SessionStorage
import com.example.retrofitandflow.common.data.repository.WebRepository
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

// TODO Implement in first activity and rename to base view model
class StartupViewModel(app: Application) : AndroidViewModel(app) {

    private val repository = WebRepository()

    fun sessionLifeCheck() {
        // TODO check session and update
    }

    fun updateSession() {
        viewModelScope.launch {
            repository.getItems2()
                    .catch {  it.printStackTrace() /* error */
                        // Logout
                    }
                    .collect {
                        SessionStorage.session = "new session"
                        SessionStorage.save(getApplication())
                    }
        }
    }
}