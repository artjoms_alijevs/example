package com.example.retrofitandflow.test

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.retrofitandflow.common.data.models.TestItem
import com.example.retrofitandflow.common.data.repository.WebRepository
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    private val repository = WebRepository()

    private val _items = MutableLiveData<List<TestItem>>()
    val items: LiveData<List<TestItem>> = _items

    // Non nullable sate (!)
    // MB implement in future with state (MVI) architecture
    private val _itemsState = MutableStateFlow<List<TestItem>>(emptyList())
    val itemsState: StateFlow<List<TestItem>> = _itemsState
    val itemsState2: StateFlow<List<TestItem>> = _itemsState

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> get() = _loading

    fun getListCustomer() {
        viewModelScope.launch {
            repository.getItems2()
                .onStart { _loading.postValue(true) }
                .onCompletion { _loading.postValue(false) }
                .catch {  it.printStackTrace() /* error */ }  // TODO investigate and think about where catch error
                .collect { _items.postValue(it) }
        }
    }

    fun getListCustomerUsingStateFlow() {
        viewModelScope.launch {
            repository.getItems2()
                .onStart { _loading.postValue(true) }
                .onCompletion { _loading.postValue(false) }
                .catch {  it.printStackTrace() /* error */ }  // TODO investigate and think about where catch error
                .collect { _itemsState.value = it }
        }
    }
}