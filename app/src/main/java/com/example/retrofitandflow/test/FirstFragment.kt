package com.example.retrofitandflow.test

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.example.retrofitandflow.R
import com.example.retrofitandflow.common.startup.TestMyKoinViewModel
import com.example.retrofitandflow.common.viewBinding
import com.example.retrofitandflow.databinding.FragmentFirstBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment(R.layout.fragment_first) {

    private val viewModel: MainViewModel by activityViewModels()
    private val viewModelNotShared: MainViewModel by viewModels()

    private val koinTestViewModel: TestMyKoinViewModel by sharedViewModel()
    private val koinTestViewModelShared: TestMyKoinViewModel by viewModel()

    private val bind by viewBinding(FragmentFirstBinding::bind)

    // TODO view binding update https://zhuinden.medium.com/simple-one-liner-viewbinding-in-fragments-and-activities-with-kotlin-961430c6c07c
    // OR can be used https://github.com/androidbroadcast/ViewBindingPropertyDelegate/blob/master/vbpd/vbpd-full/src/main/java/by/kirich1409/viewbindingdelegate/FragmentViewBindings.kt
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewModel()
        setupListeners()
    }

    private fun setupViewModel() {
        viewModel.loading.observe(viewLifecycleOwner) { show ->
            bind.progressBar.visibility = if (show) View.VISIBLE else View.GONE
        }
        viewModel.items.observe(viewLifecycleOwner) { items ->
            items.forEach { Log.d("getListCustomer result:", it.toString()) }
        }
        viewModel.getListCustomer()

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.itemsState.collect { Log.d("getListCustomerUsingStateFlow s result:", it.toString()) }
            viewModel.itemsState2.collect { Log.d("itemsState2 s result:", it.toString()) }
        }
        viewModel.getListCustomerUsingStateFlow()
    }

    private fun setupListeners() {
        // TODO fragment navigation investigation
        bind.buttonFirst.setOnClickListener {
            Log.d("test", koinTestViewModel.test)
            Log.d("check", koinTestViewModel.id.toString())
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
        }
    }
}